#include "wallfollow.hpp"

int main(int argc,char ** argv){
  ros::init(argc,argv,"lab3");
  ros::NodeHandle n;
  lab3::WallFollow wf(n);
  ros::spin();
  return 0;
}
