#pragma once
#include <ros/ros.h>
#include <std_msgs/UInt8.h> // include for switch
#include <cardriver/infrared.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>  //command publish message type

namespace lab3 { //namespance lab3

class WallFollow
{
private:
  //callback functions:
  void switchCallback(const std_msgs::UInt8::ConstPtr& msg);
  void infraredCallback(const cardriver::infrared::ConstPtr& msg);
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);
  //private variables:
  bool switches[6] = {0,0,0,0,0,0};
public:
  //functions
  WallFollow(ros::NodeHandle& nh);
  //variables
  ros::Publisher cmdPub;  // command publisher
  ros::Subscriber swSub, irSub, imuSub; // subscribers
};

} // end of namespace lab3


