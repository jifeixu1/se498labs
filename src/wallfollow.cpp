#include "wallfollow.hpp"

namespace lab3 {

WallFollow::WallFollow(ros::NodeHandle &nh){
  //initialize subscriber for each sensor topic, callback functions are
  //class private functions, 'this' is the pointer to this specific instance
  //a class variable.
  swSub = nh.subscribe("/ti/switches",10,&WallFollow::switchCallback,this);
  irSub = nh.subscribe("/ti/infrared",10,&WallFollow::infraredCallback,this);
  imuSub = nh.subscribe("/ti/imu",10,&WallFollow::imuCallback,this);

  //control message is published as a twist geometry mesasge type
  cmdPub = nh.advertise<geometry_msgs::Twist>("/ti/cartCtrl",1);

}

void WallFollow::switchCallback(const std_msgs::UInt8::ConstPtr &msg){

}

void WallFollow::infraredCallback(const cardriver::infrared::ConstPtr &msg){

}

void WallFollow::imuCallback(const sensor_msgs::Imu::ConstPtr &msg){

}

} // end of namespace lab3
